################# Below comments by nidhi####################
#this file takes input from transaction.R
#it gets rid of zero value and zero quantity line items
#original transaction files had 2595732 lines, john file had 2571753 line, output from this file(trans) will be 2552836 lines
#create the input for RFM calculations in CSV and RDATA format
##########################################################

library(dplyr)

#not used anymore
#trans_org <- read.csv(  "transaction_data.csv"
#                    , header=T
#                    , na.strings=c("")
#                    #, stringsAsFactors = T
#)
#original trans file had 2595732 transaction
select_rfm_input <- function(
  transFilenamePrefix, 
  rfmInputFilenamePrefix,
  exclude_free_items = TRUE, 
  ignore_returned_baskets = TRUE,
  compat = TRUE)
{
  #load(file="trans.RData")
  trans <- readRDS(file = paste0(transFilenamePrefix, ".RDS"))
  #this is the input from John's file(2571753), we have lost 23979 lines of transactions

  key_cols <- c("household", "day", "week", "store")
  names(key_cols) = key_cols
  value_cols <- c('paid', 'value', 'coupond1', 'coupond2', 'retaild')

  if (exclude_free_items) {
    trans <- trans %>% filter(get('value') != 0)
  }

  per_customer_per_day_data <-
    trans %>%
    mutate(purchases = sign(get('quantity'))) %>%
    mutate_at(value_cols, funs(.*get('purchases'))) %>%
    mutate(returns = 1 - get('purchases')) %>%
    group_by_at(key_cols) %>%
    summarize_at(c(value_cols, "quantity", "purchases", "returns"), sum) %>%
    mutate(date = as.Date(get('day'), origin = "2013-12-31")) %>%
    ungroup()

  if (ignore_returned_baskets)
  {
    per_customer_per_day_data <-
      per_customer_per_day_data %>%
      filter(get('purchases') != 0)
  }
  
  if (compat) {
    per_customer_per_day_data <-
      per_customer_per_day_data %>%
      select_at(c(key_cols, "paid", "quantity", 'date'))
    
  }
    
  write.csv(per_customer_per_day_data, file = paste0(rfmInputFilenamePrefix, ".csv"), row.names = compat)
  rfm_input <- per_customer_per_day_data
  # write out the data frame
  save(rfm_input, file = paste0(rfmInputFilenamePrefix, ".RDS"))
}

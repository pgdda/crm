library(dplyr)



#####comments below by Nidhi#######################
#this file takes its input from rfm inputs
#it outputs basic RFM features for customers for given time interval in "rfm_feaures.csv"
#it also ranvks the feature value from 1 to 5 in "rfm_ranking"
#######################################################

rfm_extract_features <- function(inputFilename,
                                 startDate,
                                 endDate,
                                 featuresFilename) {
  input <- read.csv(paste0(inputFilename, ".csv"), header = TRUE)
  str(input)
  #get rid og first column, gets added when we write data to a csv file
  input <- input[, c(2:8)]
  names(input) <-
    c("household",
      "day",
      "week",
      "store",
      "amount",
      "no_items",
      "date")
  str(input)
  summary(input)
  trans <- input[, c("household", "amount", "no_items", "date")]
  dim(trans)
  str(trans)
  trans$date <- as.Date(trans$date)
  str(trans)
  summary(trans)
  #checking how many customers do we have
  unique_household <- trans[!duplicated(trans[, "household"]), ]
  
  dim(unique_household)
  
  
  #subset of original dataset, lets consider data from May-Oct
  trans <- trans[trans$date >= startDate & trans$date <= endDate, ]
  
  unique_household <- trans[!duplicated(trans[, "household"]),]
  dim(unique_household)
  
  #in first 6 months we still have 2492 customes, thats good
  #when dates are changed , we have 2401 customers from may to oct
  #now we have changed the dates as second half of the year. There
  #are no comments below about how many customer we lose for this period
  
  # (hide NSE from RStudio static analysis)
  
  #https://www.kaggle.com/hendraherviawan/customer-segmentation-using-rfm-analysis-r/notebook
  trans_RFM <- trans %>%
    group_by_at("household") %>%
    summarise(
      recency = as.numeric(endDate - max(get('date'))),
      length = as.numeric(max(date) - min(get('date'))),
      frequency = n_distinct(get('date')),
      monitary = sum(get('amount')) / n_distinct(get('date')),
      basket_size = sum(get('no_items')) / n_distinct(get('date'))
    )
  summary(trans_RFM)
  str(trans_RFM)
  #length represents loyalty
  #calculating another feature,type of customer
  trans_RFM$type <- (trans_RFM$monitary) / trans_RFM$basket_size
  summary(trans_RFM)
  RFM <- trans_RFM
  
  
  write.csv(RFM, file = paste0(fe, ".csv"))
  saveRDS(RFM, file = paste0(featuresFilename, ".RDS"))
  save(RFM, file = paste0(featuresFilename, ".RData"))
}


rfm_feature_stats <- function(featuresFilename) {
  RFM <- readRDS(paste0(featuresFilename, ".RDS"))

  ######################################################
  #calculating the mean/median of feature
  
  (median_feature_value <-
     as.data.frame(sapply(RFM[, (2:7)], FUN = median)))
  (mean_feature_value <- as.data.frame(sapply(RFM[, (2:7)], FUN = mean)))
  (max_feature_value <- as.data.frame(sapply(RFM[, (2:7)], FUN = max)))
  (min_feature_value <- as.data.frame(sapply(RFM[, (2:7)], FUN = min)))

  feature_stats <- cbind(max_feature_value,
                         min_feature_value,
                         mean_feature_value,
                         median_feature_value)
  names(feature_stats) <- c("max", "min", "mean", "median")
  str(feature_stats)
  #for period 2
  (feature_stats)

  summary(RFM)
}


#startDate <- as.Date("20140101","%Y%m%d")
#endDate <- as.Date("20140630","%Y%m%d")

startDate <- as.Date("20140701", "%Y%m%d")
endDate <- as.Date("20141231", "%Y%m%d")
rfm_extract_features("rfm_input", startDate, endDate, "rfm_features")
rfm_feature_stats("rfm_features")
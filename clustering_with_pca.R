library(ggfortify)
library(clusterSim)
library(fpc)
library(dbscan)
library(mclust)

################# Below comments by nidhi####################
#this takes rfm_features.csv, does PCA and clusters using two dimensions
#i have used same features as the previous clustering. Including recency, length and
#type, made clustering worse.
##########################################################


rfm_pca_clustering <- function(featuresFilename, clustersFilename) {
  input <- read.csv(paste0(featuresFilename, ".csv"), header = TRUE)
  str(input)
  RFM <- input[, -1]
  #RFM$household <-NULL   #getting rid og this for clustering
  
  summary(RFM[, c(2:7)])
  boxplot(RFM[, c(2:7)])
  
  #checking the distributions of RFm features before and after taking the outliers out
  par(mfrow = c(3, 2))
  hist(RFM$recency, breaks = 80)
  outliers_recency <-
    (RFM$recency > (mean(RFM$recency) + (2 * sd(RFM$recency))) |
       RFM$recency < (mean(RFM$recency) - (2 * sd(RFM$recency))))
  sum(outliers_recency)
  
  RFM_OutliersRemoved <- RFM[!outliers_recency, ]
  hist(RFM_OutliersRemoved$recency, breaks = 80)
  
  hist(RFM$length)
  outliers_length <-
    (RFM$length > (mean(RFM$length) + (2 * sd(RFM$length))) |
       RFM$length < (mean(RFM$length) - (2 * sd(RFM$length))))
  sum(outliers_length)
  #170 for May-Oct, 169 removed after john's code
  RFM_OutliersRemoved <- RFM[!outliers_length, ]
  hist(RFM_OutliersRemoved$length)
  
  hist(RFM$frequency)
  outliers_frequency <-
    (RFM$frequency > (mean(RFM$frequency) + (2 * sd(RFM$frequency))) |
       RFM$frequency < (mean(RFM$frequency) - (2 * sd(RFM$frequency))))
  sum(outliers_frequency)
  #124 outliiers removed
  #133 removed from may-oct, 131 after John's code
  RFM_OutliersRemoved <- RFM[!outliers_frequency, ]
  
  hist(RFM_OutliersRemoved$frequency)
  
  hist(RFM$basket_size)
  outliers_basket_size <-
    (RFM$basket_size > (mean(RFM$basket_size) + (2 * sd(RFM$basket_size))) |
       RFM$basket_size < (mean(RFM$basket_size) - (2 * sd(RFM$basket_size))))
  sum(outliers_basket_size)
  #136 outliiers removed from May-Oct, 107 after johns code
  RFM_OutliersRemoved <- RFM[!outliers_basket_size, ]
  hist(RFM_OutliersRemoved$basket_size)
  
  
  
  hist(RFM$monitary)
  outliers_monitary <-
    (RFM$monitary > (mean(RFM$monitary) + (2 * sd(RFM$monitary))) |
       RFM$monitary < (mean(RFM$monitary) - (2 * sd(RFM$monitary))))
  sum(outliers_monitary)
  #112 ouliers removed May-OCt, 114 after john's code
  RFM_OutliersRemoved <- RFM[!outliers_monitary, ]
  hist(RFM_OutliersRemoved$monitary)
  
  hist(RFM$type)
  outliers_type <-
    (RFM$type > (mean(RFM$type) + (2 * sd(RFM$type))) |
       RFM$type < (mean(RFM$type) - (2 * sd(RFM$type))))
  sum(outliers_type)
  #116 after john's code
  RFM_OutliersRemoved <- RFM[!outliers_type, ]
  hist(RFM_OutliersRemoved$type)
  
  
  #combining three types of outliers
  outliers <-
    outliers_recency |
    outliers_frequency |
    outliers_monitary |
    outliers_basket_size | outliers_length | outliers_type
  outliers_subset <-
#    outliers_frequency | outliers_monitary | outliers_basket_size
    outliers_frequency |
    outliers_monitary | outliers_basket_size | outliers_type
  #different experiments showd these to be good features
  sum(outliers)
  sum(outliers_subset)
  #total 274  outliers removed with 3, 290 with type
  #total 550 removed, May to Oct and 2 additional parameters
  #486 removed after all 6 features are added with johns data
  RFM_OutliersRemoved <- RFM[!outliers_subset, ]
  #RFM_OutliersRemoved <- RFM[!outliers,]
  
  
  par(mfrow = c(3, 1))
  #hist(RFM_OutliersRemoved$length)
  #hist(RFM_OutliersRemoved$recency)
  hist(RFM_OutliersRemoved$frequency)
  hist(RFM_OutliersRemoved$basket_size)
  hist(RFM_OutliersRemoved$monitary)
  
  
  
  RFM1 <- RFM_OutliersRemoved  #creating a copy
  summary(RFM1)
  summary(RFM)
  
  str(RFM1)
  #if we decide to use only three feature
  
  RFM2 <- RFM1[, c(4, 5, 6)]
  RFM3 <- RFM1[, c(1, 4, 5, 6)]
  #RFM3 is preserved for later customer clusters identification, its not processed after this step
  # JSL - write out RFM3 : commented so do it manually from here if you need it
  # save( RFM3, file = "RFM3_PCA.RData" )
  # same a Kmeans versions - but saveing under a different name just in case
  # outlier treatment changes
  summary(RFM2)
  summary(RFM3)
  #RFM2$frequency=RFM2$frequency * RFM2$frequency
  #RFM2$monitary=RFM2$monitary * RFM2$monitary
  #RFM2$basket_size=RFM2$basket_size * RFM2$basket_size
  
  
  

  normalize <-
    function(x) {
      return((x - min(x)) / (max(x) - min(x)))
    }
  
  par(mfrow = c(1, 1))
  #RFM2_normalize <- normalize(RFM2)
  RFM2_normalize <- as.data.frame(lapply(RFM2, normalize))
  summary(RFM2_normalize)
  boxplot(RFM2_normalize)
  autoplot(prcomp(RFM2_normalize, scale. = T, center = T))
  
  (prcomp(RFM2_normalize))
  screeplot(prcomp(RFM2_normalize, scale. = T, center = T), type = "lines")
  
  library(factoextra) #Extract and visualise the Multivariate analysis
  library(FactoMineR) #Multivariate Exploratory Data Analysis and Data Mining with R
  
  #I have verified that scaling and centering gives us the best results.
  pca <- prcomp(RFM2_normalize, scale. = T, center = T)
  fviz_screeplot(pca, addlabels = TRUE)
  str(pca)
  (eig <- (pca$sdev) ^ 2)
  (pca$rotation)
  set.seed(1337)
  res.1 <- kmeans(pca$x[, 1:2], 8, nstart = 50)
  
  # JSL - save the output of the clusters - again commented as not needed every time
  # RFMclusters <- res.1$cluster
  # save( RFMclusters, file = "RFMclusters_PCA.RData" )
  autoplot(res.1, data = pca)
  
  res.1$iter
  

  results <- list()
  tot.withinss <- c()
  betweenss <- c()
  dbindex <- c()
  silhouettes <- c()
  #another important measure,its value shod be high
  CH <- c()
  
  for (k in 2:10) {
    results[[k]] <- kmeans(pca$x[, 1:2], k, nstart = 50)
    tot.withinss[k] <- results[[k]]$tot.withinss
    betweenss[k] <- results[[k]]$betweenss
    dbindex[k] <-
      index.DB(pca$x[, 1:2], results[[k]]$cluster, centrotypes = "centroids")$DB
    s <- silhouette(results[[k]]$cluster, daisy(pca$x))
    silhouettes[k] <- mean(s[, 3])
    CH[k] <- calinhara(pca$x[, 1:2], results[[k]]$cluster)
  }
  
  
  par(mfrow = c(2, 2))
  plot(tot.withinss, xlab = "k")
  plot(betweenss, xlab = "k")
  plot(dbindex, xlab = "k")
  
  plot(silhouettes, xlab = "k")
  
  
  par(mfrow = c(1, 1))
  
  #R was not showing any silhouettes
  windows()
  
  
  plot(silhouette(results[[8]]$cluster, daisy(pca$x)))
  dbindex[8]
  silhouettes[8]
  
  
  
  
  #plotting of clusters

  plotcluster(pca$x, res.1$cluster)
  res.1$centers
  
  centers <-
    res.1$centers[res.1$cluster,] #vector of all centers for each point
  distances <-
    sqrt(rowSums((pca$x[, 1:2] - centers) ^ 2)) #Euclidean pair-wise distances
  summary(distances)
  sd(distances)
  
  # pick n largest distances
  outliers <- order(distances, decreasing = T)[1:400]
  
  #pick outliers that are more than 2 standard deviation away
  #outliers <- (distances > (mean(distances) + (2 * sd(distances))) |
  #               distances < (mean(distances) - (2 * sd(distances))))

  print(pca$x[outliers, ])
  
  plot(pca$x[, c("PC1", "PC2")],
       pch = "0",
       col = res.1$cluster,
       cex = 0.3)
  points(res.1$centers[, c("PC1", "PC2")],
         col = 1:3,
         pch = 6,
         cex = 2.5)
  points(pca$x[outliers, c("PC1", "PC2")],
         pch = "+",
         col = 5,
         cex = 1.5)
  
  
  
  
  kmeans_pca_clusters_all <- RFM3
  kmeans_pca_clusters_all$cluster_id <- res.1$cluster
  kmeans_pca_clusters <- kmeans_pca_clusters_all[-outliers, ]
  
  
  RFM2_normalize_1 <- RFM2_normalize[-outliers, ]
  
  
  kmeans_pca_clusters$frequency_rank <-
    with(kmeans_pca_clusters, 
             as.integer(cut(
               frequency,
               quantile(frequency, probs =
                          0:5 / 5),
               include.lowest = TRUE
             )))
  kmeans_pca_clusters$monitary_rank <-
    with(kmeans_pca_clusters, 
             as.integer(cut(
               monitary,
               quantile(monitary, probs =
                          0:5 / 5), include.lowest = TRUE
             )))
  kmeans_pca_clusters$basket_size_rank <-
    with(kmeans_pca_clusters, 
             as.integer(cut(
               basket_size,
               quantile(basket_size, probs =
                          0:5 / 5),
               include.lowest = TRUE
             )))
  
  
  str(kmeans_pca_clusters)
  write.csv(kmeans_pca_clusters,
            file = paste0(clustersFilename, ".csv"))
  save(kmeans_pca_clusters, file = paste0(clustersFilename, ".RData"))
  saveRDS(kmeans_pca_clusters,
          file = paste0(clustersFilename, ".RDS"))

  aggregate(monitary ~ cluster_id, data = kmeans_pca_clusters, FUN = summary)
  aggregate(frequency ~ cluster_id, data = kmeans_pca_clusters, FUN = summary)
  aggregate(basket_size ~ cluster_id, data = kmeans_pca_clusters, FUN = summary)
  
  
  str(res.1)
  
  #plotting the clusters, FMB abd the PC1 and PC2
  plot(
    RFM2_normalize[, c("frequency", "monitary")],
    pch = "*",
    col = res.1$cluster,
    cex = 1.5
  )
  plot(
    RFM2_normalize[, c("frequency", "basket_size")],
    pch = "*",
    col = res.1$cluster,
    cex = 1.5
  )
  plot(
    RFM2_normalize[, c("monitary", "basket_size")],
    pch = "*",
    col = res.1$cluster,
    cex = 1.5
  )
  plot(pca$x,
       pch = "*",
       col = res.1$cluster,
       cex = 1.5)

  
  
  
  
  d <- dist(pca$x[, 1:2])
  fit <- hclust(d, method = 'ward.D2')
  
  plot(fit)
  
  rect.hclust(fit, k = 8, border = "red")
  
  members <- cutree(fit, k = 8)
  table(members)
  aggregate(RFM2, by = list(members), mean)
  
  
  
  ######################################################################
  ds <- fpc::dbscan(pca$x[, 1:2], eps = .02, MinPts = 5)
  table(ds$cluster)
  hullplot(pca$x[, 1:2], ds$cluster)
  #this does not work incurent state ,one big cluster with 1900 items, remaining very small
  #it works when we add two features, length and basket size
  ###########################################################################
  fit <- Mclust(pca$x[, 1:2])
  plot(fit, what = "classification")
  summary(fit)
  #gives 7 clusters, have not seen the quality of clusters
  #fit$classification
  str(fit)
}

rfm_pca_clustering("rfm_features", "kmeans_pca_clusters")